import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.*;

public class Count {

    public static class TokenizerMapper
            extends Mapper<Object, Text, Text, Text> {

        private Text one = new Text();
        private Text word = new Text();
        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {
            HashMap<String, HashMap<String, Integer>> dbMap = new HashMap<>();

            StringTokenizer itr = new StringTokenizer(value.toString(), "\n");

            while (itr.hasMoreTokens()) {
                StringTokenizer itr2 = new StringTokenizer(itr.nextToken(), ",");
                List<String> list = new ArrayList<>();
                while (itr2.hasMoreTokens()) {
                    String column = itr2.nextToken();
                    list.add(column);
                }

                try {
                    String yearsCodedJob = list.get(3);
                    String[] split = yearsCodedJob.split(" ");
                    if (!Objects.equals(list.get(1), "Professional developer")
                            || Objects.equals(yearsCodedJob, "NA")
                            ||Integer.parseInt(split[0]) < 3) {
                        continue;
                    }
                } catch (NumberFormatException ex) {
                    continue;
                }

                String dbList = list.get(60);
                String languageList = list.get(56);

                String[] splitLanguage = languageList.split(";");
                String[] splitDb = dbList.split(";");
                for (String s : splitLanguage) {
                    if (Objects.equals(s, "NA")) {
                        continue;
                    }

                    String language = s.trim();

                    dbMap.put(language, new HashMap<String, Integer>());

                    for (String s2 : splitDb) {
                        if (Objects.equals(s2, "NA")) {
                            continue;
                        }
                        String db = s2.trim();

                        if (!dbMap.containsKey(language)) {
                            HashMap<String, Integer> map = new HashMap<>();
                            map.put(db, 1);
                            dbMap.put(language, map);
                        } else {
                            HashMap<String, Integer> map = dbMap.get(language);
                            if (map.containsKey(db)) {
                                Integer integer = map.get(db);
                                map.put(db, integer + 1);
                            } else {
                                HashMap<String, Integer> map1 = new HashMap<>();
                                map1.put(db, 1);
                                dbMap.put(language, map1);
                            }
                        }
                    }
                }
            }
//60
            for (Map.Entry<String, HashMap<String, Integer>> entry : dbMap.entrySet()) {
                word.set(entry.getKey());
                HashMap<String, Integer> hashMap = dbMap.get(entry.getKey());
                for (Map.Entry<String, Integer> stringIntegerEntry : hashMap.entrySet()) {
                    String s =
                            stringIntegerEntry.getKey() + ";" + stringIntegerEntry.getValue();
                    one.set(s);
                    context.write(word, one);
                }
            }
        }
    }

    public static class IntSumReducer
            extends Reducer<Text,Text,Text,Text> {
        public void reduce(Text key, Iterable<Text> values,
                           Context context
        ) throws IOException, InterruptedException {

            HashMap<String, Integer> hashMap = new HashMap<>();
            for (Text val : values) {
                StringTokenizer itr = new StringTokenizer(val.toString(), ";");

                String db = itr.nextToken();
                String cout = itr.nextToken();

                if (hashMap.containsKey(db)) {
                    Integer integer = hashMap.get(db);
                    hashMap.put(db, integer + Integer.parseInt(cout));
                } else {
                    hashMap.put(db, Integer.parseInt(cout));
                }
            }

            for (Map.Entry<String, Integer> entry : hashMap.entrySet()) {
                String s = entry.getKey() + ";" + entry.getValue();
                context.write(key, new Text(s));
            }
        }
    }


    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "word count");
        job.setJarByClass(Count.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));

        FileSystem.get(conf).delete(new Path(args[1]), true);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}